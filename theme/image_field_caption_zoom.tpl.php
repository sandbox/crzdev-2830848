<?php
/**
 * $image - contains the image html rendered by Drupal
 * $caption - contains the image field caption string
 * $path - image path
 */
?>
<div class="ifcz-image-field">
  <div class="zoom-wrapper">
    <div>
      <?php print $image; ?>
    </div>
  </div>
  <div class="image-field-caption">
    <?php print $caption; ?>
  </div>
</div>
