/**
 * @file
 * Javacript file
 */
(function ($, Drupal, window, document, undefined) {

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.image_field_caption_zoom = {
    attach: function(context, settings) {
      // Init.
      setImagesZoom('.ifcz-image-field .zoom-wrapper');
    }
  };

  function setImagesZoom(element, context) {
    $(element, context).once('ifcz-init', function () {
      $(this).peakzoom();
    });
  }

})(jQuery, Drupal, this, this.document);
